# nrfusb-electronics

The nrfusb is an usb adapter to make the nRF24L01 radio to work directly from the computer, without the need of

- making any electrical connections with (disgusting) [dupont wires](https://findanyanswer.com/what-is-a-dupont-wire "dupont wires")
- getting angry because apparently your radio is broken but you only forgot to solder some crazy bypass capacitors [see the story here](https://bigdanzblog.wordpress.com/2015/02/21/the-very-frustrating-nrf24l01/ "see the story here")
- having to use non open source software and hardware

## How to I get it working?

If you do not want to solder anything, you are not doomed! Although the proposed electronic board is great because it protects you from getting frustrated with fake nrf24l01 radios, you may use the firmware with a [Bluepill](https://stm32world.com/wiki/Blue_Pill "Bluepill"). Just by one Bluepill, upload the firmware and connect the radio to it using dupont wires.

But if you are a true maker, then you will fall in love with this project ❤️. The whole thing is open source, including the software used to design it:

- the board design files (made with [Kicad](https://www.kicad.org "Kicad"))
- the bill of materials (made with [Interactive HTML BOM plugin for KiCad](https://github.com/openscopeproject/InteractiveHtmlBom "Interactive HTML BOM plugin for KiCad") and we only use components that may be bought from AliExpress)
- a nice and cozy case that can be 3d printed (made with [FreeCAD](https://www.freecadweb.org/ "FreeCAD"))
- the microcontroller firmware (that currently uses STM32CubeIDE but I want to use CMake + any other IDE that it is not [eclipse based](https://www.reddit.com/r/java/comments/2jona4/why_does_everyone_hate_eclipse/ "eclipse based"))
- the low level computer interface is built with modern (but understandable) C++ and CMake, but it includes a high level API to be used with Python (and maybe other languages some day)

### Differences with other usb adapters

The nrfusb is smart and not an spi adapter. The computer doesn't see the radio, it just tells to the mcu what to do and it will do it! The communication between the computer and the mcu uses an emulated serial interface through a 2.0 usb.

There are some usb adapters already, but, to best of the author knowledge (and I tried some of them), they

- lack documentation
- some of them use some usb to serial chip adapter, what may introduce latency delays in the communication - [see the story here](https://askubuntu.com/questions/696593/reduce-request-latency-on-an-ftdi-ubs-to-rs-232-adapter "see the story here")
- there are also some usb2spi adapters, but you will face closed source libraries and random delays, what makes the communication with the radio something very painful


## Current issues and ideas

### Issues of REV1:

- the board too big: it does not fit inside the metallic case of a STLINK/v2 clone
- the swd connector hurts people: the pin header must be replaced with JST_SH_BM05B-SRSS-TB_1x05-1MP_P1.00mm_Vertical
- the radio adapter placement can be improved: place it like the connector of the STLINK/v2 clone
- the 3.3 v regulator is too big
- the 4 pins of the usb connector type A should be surface mount to save space
- the leds should be placed in the board edge
- the on led should be connected to the microcontroller, so the user will know that the microcontroller is working

### To think about

- a new led may be added to indicate that the radio was found (and working!)
- to think about: maybe it is useful to add a regulator only to the radio: the idea is to measure the current and prevent a short to damage the board


